<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Budget extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function catalogue()
    {
        return $this->hasOne('App\Catalogue','id','catalogue_id');
    }
}
