<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{

    use SoftDeletes;

    /**
     *
     **/
    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }


    /**
     *
     **/
    public function order()
    {
        return $this->hasOne('App\Order','id','order_id');
    }


    /**
     *
     **/
    public function catalogue()
    {
        return $this->hasOne('App\Catalogue','id','catalogue_id');
    }
}
