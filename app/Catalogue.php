<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Catalogue extends Model
{
    use SoftDeletes;


    /**
     *
     **/
    public function budget()
    {
        return $this->hasOne('App\Budget','catalogue_id','id');
    }

    /**
     *
     **/
    public function drug()
    {
        return $this->hasOne('App\Drug','id','drug_id');
    }

    /**
     *
     **/
    public function cart()
    {
        return $this->hasOne('App\Cart','catalogue_id','id');
    }

    /**
     *
     **/
    public function shop()
    {
        return $this->hasOne('App\Shop','id','shop_id');
    }
}
