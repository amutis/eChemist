<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;
    /**
     *
     **/
    public function manufacturers()
    {
        return $this->hasMany('App\Manufacturer','country_id','id');
    }

    /**
     *
     **/
    public function shops()
    {
        return $this->hasMany('App\Shop','country_id','id');
    }

    /**
     *
     **/
    public function orders()
    {
        return $this->hasMany('App\Order','country_id','id');
    }
}
