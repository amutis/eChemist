<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Drug extends Model
{
    use SoftDeletes;
    /**
     *
     **/
    public function generic()
    {
        return $this->hasOne('App\Generic','id','generic_id');
    }

    /**
     *
     **/
    public function side_effect()
    {
        return $this->hasOne('App\SideEffect','drug_id','id');
    }

    /**
     *
     **/
    public function catalogues()
    {
        return $this->hasMany('App\Catalogue','drug_id','id');
    }

    /**
     *
     **/
    public function manufacturer()
    {
        return $this->hasMany('App\Manufacturer','id','manufacturer_id');
    }
}
