<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Generic extends Model
{
    use SoftDeletes;
    /**
     *
     **/
    public function drugs()
    {
        return $this->hasMany('App\Drug','generic_id','id');
    }


}
