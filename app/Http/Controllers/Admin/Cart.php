<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Cart extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     *
     **/
    public function getUnCheckedOut()
    {
        return view('admin.pages.cart');
    }

    /**
     *
     **/
    public function getCheckedOut()
    {
        return view('admin.pages.cart');
    }
}
