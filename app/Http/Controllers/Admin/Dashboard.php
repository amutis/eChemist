<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Dashboard extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     **/
    public function dashboard()
    {
        return view('admin.pages.dashboard');
    }

    /**
     *
     **/
    public function getUserCreate()
    {
        return view('admin.pages.create_user');
    }

    /**
     *
     **/
    public function getUsers()
    {
        return view('admin.pages.users');
    }
}
