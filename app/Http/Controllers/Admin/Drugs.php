<?php

namespace App\Http\Controllers\Admin;

use App\Drug;
use App\Manufacturer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Drugs extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     *
     **/
    public function getManufacturerDrugs($id)
    {
        $id = decrypt($id);

        $manufacturer = Manufacturer::find($id);
        $drugs = Drug::where('manufacturer_id',$id)->get();

        return view('admin.pages.drugs')
            ->withDrugs($drugs)
            ->withManufacturer($manufacturer);
    }
}
