<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class Generic extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     *
     **/
    public function getCreate()
    {
        return view('admin.pages.add_generic');
    }

    /**
     *
     **/
    public function postGeneric(Request $request)
    {
        $this->validate($request, array(
            'generic_name' => 'required|unique:generics,name',
            'description' => 'required',
        ));

        $generic = new \App\Generic();
        $generic->name = $request->generic_name;
        $generic->description = $request->description;
        $generic->save();

        return redirect()->route('admin.generics');
    }

    /**
     *
     **/
    public function getGenerics()
    {
        return view('admin.pages.generics');
    }

    /**
     *
     **/
    public function getDeletedGenerics()
    {
        return view('admin.pages.generics');
    }


    /**
     *
     **/
    public function deleteGeneric($id)
    {
        //Decryption of ID
        $id = decrypt($id);

        \App\Generic::find($id)->delete();

        Session::flash('success', 'User has been deleted.');
        return redirect()->route('admin.deleted_generics');
    }

    /**
     *
     **/
    public function reinstateGeneric($id)
    {
        //Decryption of ID
        $id = decrypt($id);

        \App\Generic::withTrashed()->find($id)->restore();

        Session::flash('success', 'User reinstated.');
        return redirect()->route('admin.generics');
    }
}
