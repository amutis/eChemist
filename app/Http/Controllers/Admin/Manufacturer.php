<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class Manufacturer extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     *
     **/
    public function getCreateManufacturer()
    {
        return view('admin.pages.create_manufacturer');
    }


    /**
     *
     **/
    public function getDeletedManufacturer()
    {
        return view('admin.pages.manufacturers');
    }


    /**
     *
     **/
    public function postManufacturer(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'country' => 'required',
            'address' => 'required',
            'postal_code' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'telephone' => 'required',
            'manufacturer_email' => 'required|email|unique:manufacturers,email',
            'manager_email' => 'required|email|unique:users,email',
        ));

        $manufacturer = new \App\Manufacturer();
        $manufacturer->name = $request->name;
        $manufacturer->country_id = decrypt($request->country);
        $manufacturer->address = $request->address;
        $manufacturer->postal_code = $request->postal_code;
        $manufacturer->email = $request->manufacturer_email;
        $manufacturer->telephone_1 = $request->telephone;
        $manufacturer->save();

        //password generation
        $password = "123456";

        //production password generation
//        $password = Str::random(8);

        $user = new User();
        $user->user_type_id = 3;
        $user->institution_id = $manufacturer->id;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->manager_email;
        $user->password = bcrypt($password);
        $user->save();

        Session::flash('success', 'User has been added');

        return redirect()->route('admin.manufacturers');

    }

    /**
     *
     **/
    public function getManufacturers()
    {
        return view('admin.pages.manufacturers');

    }



}
