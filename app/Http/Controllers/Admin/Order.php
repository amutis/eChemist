<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Order extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     *
     **/
    public function getUncomplete()
    {
        return view('admin.pages.orders');
    }

    /**
     *
     **/
    public function getComplete()
    {
        return view('admin.pages.orders');
    }
}
