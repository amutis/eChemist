<?php

namespace App\Http\Controllers\Admin;

use App\ShopType;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class Shop extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     **/
    public function getCreateShop()
    {
        return view('admin.pages.create_shop');
    }

    /**
     *
     **/
    public function postShop(Request $request)
    {
        $this->validate($request, array(
            'shop_name' => 'required',
            'shop_type' => 'required',
            'country' => 'required',
            'town' => 'required',
            'region' => 'required',
            'street' => 'required',
            'telephone_1' => 'required|max:12|min:12|unique:shops',
            'building' => 'required',
            'shop_email' => 'required|email|unique:shops,email',
            'first_name' => 'required',
            'last_name' => 'required',
            'manager_email' => 'required|email|unique:users,email',
        ));

        $shop = new \App\Shop();
        $shop ->shop_type_id = decrypt($request->shop_type);
        $shop ->country_id = decrypt($request->country);
        $shop ->name = $request->shop_name;
        $shop ->town = $request->town;
        $shop ->region = $request->region;
        $shop ->street = $request->street;
        $shop ->telephone_1 = $request->telephone_1;
        if ($request->telephone_1 != null){
            $shop ->telephone_2 = $request->telephone_2;
        }
        if ($request->website != null){
            $shop ->website = $request->website;
        }
        $shop ->building = $request->building;
        $shop ->email = $request->shop_email;
        $shop->save();

        //password generation
        $password = "123456";

        //production password generation
//        $password = Str::random(8);

        $user = new User();
        $user->user_type_id = 2;
        $user->institution_id = $shop->id;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->manager_email;
        $user->password = bcrypt($password);
        $user->save();

        Session::flash('success', 'Shop and User have been registered.');
        return redirect()->route('admin.shops');
    }

    /**
     *
     **/
    public function getShop($id)
    {
        $id = decrypt($id);
        return \App\Shop::find($id);
    }

    /**
     *
     **/
    public function getShops()
    {
        return view('admin.pages.shops');
    }


    /**
     *
     **/
    public function getDeletedShops()
    {
        return view('admin.pages.shops');
    }

    /**
     *
     **/
    public function deleteShop($id)
    {
        $id = decrypt($id);
        \App\Shop::find($id)->delete();

        Session::flash('success', 'Shop has been deleted');
        return redirect()->route('admin.deleted_shops');
    }

    /**
     *
     **/
    public function reinstateShop($id)
    {
        //Decryption of ID
        $id = decrypt($id);

        \App\Shop::withTrashed()->find($id)->restore();

        Session::flash('success', 'Shop reinstated.');
        return $user = \App\Shop::find($id);
    }

    /**
     *
     **/
    public function getShopTypes()
    {
        return view('admin.pages.shop_types');
    }

    /**
     *
     **/
    public function postShopType(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'description' => 'required',
        ));

        $shop_type = new ShopType();
        $shop_type->name = $request->name;
        $shop_type->description = $request->description;
        $shop_type->save();

        Session::flash('success', $request->name.' has been added.');
        return redirect()->route('admin.shop_types');

    }
}
