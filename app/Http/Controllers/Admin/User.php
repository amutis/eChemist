<?php

namespace App\Http\Controllers\Admin;

use App\UserType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class User extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     *
     **/
    public function postUser(Request $request)
    {
        $this->validate($request, array(
            'user_type' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
        ));

        //password generation
        $password = "123456";

        //production password generation
//        $password = Str::random(8);

        $user = new \App\User();
        $user->user_type_id = decrypt($request->user_type);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = bcrypt($password);
        $user->save();

        Session::flash('success', 'User has been added');
        return redirect()->route('admin.users');
    }

    /**
     *
     **/
    public function getUser($id)
    {
        //Decryption of ID
        $id = decrypt($id);

        return $user = \App\User::find($id);
    }

    /**
     *
     **/
    public function getUserTypes()
    {
        return view('admin.pages.user_types');
    }

    /**
     *
     **/
    public function postUserType(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'description' => 'required',
        ));

        $user_type = new UserType();
        $user_type->name = $request->name;
        $user_type->description = $request->description;
        $user_type->save();

        Session::flash('success', $request->name.' has been added');
        return redirect()->route('admin.user_types');
    }

    /**
     *
     **/
    public function deletedUser()
    {
        return view('admin.pages.users');
    }

    /**
     *
     **/
    public function deleteUser($id)
    {
        //Decryption of ID
        $id = decrypt($id);

        \App\User::find($id)->delete();

        Session::flash('success', 'User has been deleted.');
        return redirect()->route('admin.deleted_users');
    }

    /**
     *
     **/
    public function reinstateUser($id)
    {
        //Decryption of ID
        $id = decrypt($id);

        \App\User::withTrashed()->find($id)->restore();

        Session::flash('success', 'User reinstated.');
        return $user = \App\User::find($id);
    }


}
