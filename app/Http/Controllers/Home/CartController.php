<?php

namespace App\Http\Controllers\Home;

use App\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    /**
     *
     **/
    public function getCart()
    {
        $cart_count = Cart::where('user_id',Auth::user()->ic)->where('status',0)->count();

        $user = Auth::user()->id;

        $cart = Cart::select(DB::raw('sum(quantity*price) AS total'))->where('user_id',$user)->where('status',0)->get();
        $cart = str_replace('[{"total":"','',$cart);
        $total = str_replace('"}]','',$cart);

        return view('home.pages.cart')->withTotal($total);
    }



}
