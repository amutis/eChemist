<?php

namespace App\Http\Controllers\Home;

use App\Cart;
use App\Catalogue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Jquey extends Controller
{
    /**
     *
     **/
    public function addToCart($id)
    {
        $id = decrypt($id);
        $cart = new Cart();
        $cart->user_id = Auth::user()->id;
        $cart->catalogue_id = $id;
        $cart->price = Catalogue::find($id)->budget->selling_price;
        $cart->quantity = 1;
        $cart->save();


    }

    /**
     *
     **/
    public function countCart()
    {
        $user = Auth::user();
        $cart = Cart::where('user_id',$user->id)->where('status',0)->count();

        echo $cart;
    }

    /**
     *
     **/
    public function cart_subtract($id)
    {
        $id = decrypt($id);

        $least = Cart::find($id);
        if($least->quantity>1){
            Cart::find($id)->decrement('quantity');
        }
        $cart_item = Cart::find($id);
        echo $cart_item->price*$cart_item->quantity;
    }

    /**
     *
     **/
    public function cart_add($id)
    {
        $id = decrypt($id);
        Cart::find($id)->increment('quantity');
        $cart_item = Cart::find($id);
        echo $cart_item->price*$cart_item->quantity;
    }

    /**
     *
     **/
    public function cart_total()
    {
        $user_id = Auth::user()->id;

        $cart = Cart::select(DB::raw('sum(quantity*price) AS total'))->where('status',0)->where('user_id',$user_id)->get();
        $cart = str_replace('[{"total":"','',$cart);
        $cart = str_replace('"}]','',$cart);
        echo  $cart;

    }
}
