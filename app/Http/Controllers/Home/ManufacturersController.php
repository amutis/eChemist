<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManufacturersController extends Controller
{
    /**
     *
     **/
    public function all()
    {
        return view('home.pages.manufacturers');
    }
}
