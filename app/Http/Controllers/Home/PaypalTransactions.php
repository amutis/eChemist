<?php

namespace App\Http\Controllers\Home;

use App\Account;
use App\Cart;
use App\Mail;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Netshell\Paypal\Facades\Paypal;
use App\Http\Controllers\Controller;

class PaypalTransactions extends Controller
{


    private $_apiContext;

    /**
     *
     **/
    public function user()
    {
        return Auth::user();
    }

    public function __construct()
    {

        $this->middleware('auth');

        $this->_apiContext = Paypal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        ));
    }

    function sconvert($amount){

        return $amount = currency($amount, 'KES', 'USD' ,$format = false);

    }


    /**
     *
     **/
    public function getPaypalPage()
    {
        return view('home.paypal');
    }

    /**
     *
     **/
    public function post_order()
    {

        $cart = Cart::select(DB::raw('sum(quantity*price) AS total'))->where('status',0)->where('user_id',$this->user()->id)->get();
        $cart = str_replace('[{"total":"','',$cart);
        $cart = str_replace('"}]','',$cart);

        $cart_total =$cart;

        $payer = Paypal::Payer();
        $payer->setPaymentMethod('paypal');

        /**
         * Currency conversion to USD which is the default
         */

        $Amount = $this->sconvert($cart_total);

        $amount = Paypal:: Amount();
        $amount->setCurrency('USD');
        $amount->setTotal($Amount); // This is the simple way,
        // you can alternatively describe everything in the order separately;
        // Reference the PayPal PHP REST SDK for details.

        $transaction = Paypal::Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription('Deposit to dashpay');

        $redirectUrls = Paypal:: RedirectUrls();
        $redirectUrls->setReturnUrl(route('get_done',$cart_total));
        $redirectUrls->setCancelUrl(route('get_cancel'));

        $payment = Paypal::Payment();
        $payment->setIntent('order');
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));

        $response = $payment->create($this->_apiContext);
        $redirectUrl = $response->links[1]->href;

        return Redirect::to( $redirectUrl);
    }

    public function getDone(Request $request,$Amount)
    {
        $id = $request->get('paymentId');
        $token = $request->get('token');
        $payer_id = $request->get('PayerID');

        $order = new Order();
        $order->user_id = Auth::user()->id;
        $order->paypal = $id;
        $order->save();

        Cart::where('status', 0)
            ->where('user_id',Auth::user()->id)
            ->update(['status' => 1,'order_id' => $order->id]);

        return redirect()->route('home.home');
    }

    public function getCancel()
    {
        // Curse and humiliate the user for cancelling this most sacred payment (yours)
        return redirect()->route('home.paypal_deposit');
    }
}
