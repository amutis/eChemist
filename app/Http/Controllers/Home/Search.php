<?php

namespace App\Http\Controllers\Home;

use App\Drug;
use App\Generic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Search extends Controller
{
    /**
     *
     **/
    public function result()
    {
        $drugs = Drug::all();
        return view('home.pages.result')
            ->withDrugs($drugs);
    }

    /**
     *
     **/
    public function search(Request $request)
    {
        $this->validate($request, array(
            'search_term' => 'required',
        ));

        $search = new \App\Search();
        if(Auth::check()){
            $search->user_id = Auth::user()->id;
        }
        $search->term = $request->search_term;
        $search->save();

         $drugs = Drug::where('name','LIKE','%'.$request->search_term.'%')->orwhere('description','LIKE','%'.$request->search_term.'%')->get();
        $generic = Generic::where('name','LIKE','%'.$request->search_term.'%')->orwhere('description','LIKE','%'.$request->search_term.'%')->get();

        return view('home.pages.result')
            ->withDrugs($drugs)
            ->withGenerics($generic);

    }
}
