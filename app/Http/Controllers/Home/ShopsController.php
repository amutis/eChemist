<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopsController extends Controller
{
    /**
     *
     **/
    public function all()
    {
        return view('home.pages.shops');
    }
}
