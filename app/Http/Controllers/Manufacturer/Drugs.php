<?php

namespace App\Http\Controllers\Manufacturer;

use App\Drug;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class Drugs extends Controller
{
    /**
     *
     **/
    public function getCreateDrug()
    {
        return view('manufacturer.pages.create_drug');
    }

    /**
     *
     **/
    public function postDrug(Request $request)
    {
        $this->validate($request, array(
            'generic_name' => 'required',
            'name' => 'required',
            'image' => 'required|image',
            'description' => 'required',
        ));

        $image = Input::file('image');
        $filename  = time() . '.' . $image->getClientOriginalExtension();


        $path = public_path('drugs/' . $filename);


        Image::make($image->getRealPath())->save($path);

        $drug = new Drug();
        $drug->manufacturer_id = Auth::user()->institution_id;
        $drug->generic_id = decrypt($request->generic_name);
        $drug->name = $request->name;
        $drug->image = $filename;
        $drug->description = $request->description;
        $drug->save();

        return redirect()->route('manufacturer.drugs');
    }

    /**
     *
     **/
    public function getDrugs()
    {
        return view('manufacturer.pages.drugs');
    }

    /**
     *
     **/
    public function getDeletedDrugs()
    {
        return view('manufacturer.pages.drugs');
    }


    /**
     *
     **/
    public function deleteDrug($id)
    {
        //Decryption of ID
        $id = decrypt($id);

        Drug::find($id)->delete();

        Session::flash('success', 'User has been deleted.');
        return redirect()->route('manufacturer.deleted_drugs');
    }

    /**
     *
     **/
    public function reinstateDrug($id)
    {
        //Decryption of ID
        $id = decrypt($id);

        Drug::withTrashed()->find($id)->restore();

        Session::flash('success', 'User reinstated.');
        return $user = Drug::find($id);
    }
}
