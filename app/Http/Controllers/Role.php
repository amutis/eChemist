<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Role extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     **/
    public function check()
    {
        $user_type = Auth::user()->user_type_id;

        if ($user_type == 1){
            return redirect('/');
        }elseif ($user_type == 2){
            return redirect()->route('shop.dashboard');
        }elseif ($user_type == 3){
            return redirect()->route('manufacturer.dashboard');
        }
        elseif ($user_type == 4){
            return redirect()->route('admin.dashboard');
        }
    }
}
