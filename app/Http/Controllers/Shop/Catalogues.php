<?php

namespace App\Http\Controllers\Shop;

use App\Budget;
use App\Catalogue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Catalogues extends Controller
{
    /**
     *
     **/
    public function getCreateDrug()
    {
        return view('shop.pages.add_drug');
    }

    /**
     *
     **/
    public function postDrug(Request $request)
    {
        $this->validate($request, array(
            'manufacturer' => 'required',
            'drug' => 'required',
            'quantity' => 'required',
            'buying_price' => 'required',
            'selling_price' => 'required',
        ));

        $catalogue = new Catalogue();
        $catalogue->shop_id = Auth::user()->institution_id;
        $catalogue->drug_id = decrypt($request->drug);
        $catalogue->quantity = $request->quantity;
        $catalogue->save();

        $budget = new Budget();
        $budget->catalogue_id = $catalogue->id;
        $budget->buying_price = $request->buying_price;
        $budget->selling_price = $request->selling_price;
        $budget->save();

        return redirect()->route('shop.drugs');

    }

    /**
     *
     **/
    public function getDrugs()
    {
        return view('shop.pages.drugs');
    }



    /**
     *
     **/
    public function deleteDrug($id)
    {
        //Decryption of ID
        $id = decrypt($id);

        Catalogue::find($id)->delete();

        Session::flash('success', 'User has been deleted.');
        return redirect()->route('shop.deleted_drugs');
    }

    /**
     *
     **/
    public function reinstateDrug($id)
    {
        //Decryption of ID
        $id = decrypt($id);

        Catalogue::withTrashed()->find($id)->restore();

        Session::flash('success', 'User reinstated.');
        return $user = Catalogue::find($id);
    }

    /**
     *
     **/
    public function getDeletedDrugs()
    {
        return view('shop.pages.drugs');
    }
}
