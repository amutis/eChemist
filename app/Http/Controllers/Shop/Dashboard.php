<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Dashboard extends Controller
{
    /**
     *
     **/
    public function dashboard()
    {
        return view('shop.pages.dashboard');
    }
}
