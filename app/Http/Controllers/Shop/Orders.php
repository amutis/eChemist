<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Orders extends Controller
{
    /**
     *
     **/
    public function getUnComplete()
    {
        return view('shop.pages.orders');
    }

    /**
     *
     **/
    public function getComplete()
    {
        return view('shop.pages.orders');
    }
}
