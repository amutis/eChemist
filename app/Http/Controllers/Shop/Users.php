<?php

namespace App\Http\Controllers\Shop;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Users extends Controller
{
    /**
     *
     **/
    public function getUserCreate()
    {
        return view('shop.pages.create_user');
    }

    /**
     *
     **/
    public function postUser(Request $request)
    {
        $this->validate($request, array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
        ));

        //password generation
        $password = "123456";

        //production password generation
//        $password = Str::random(8);

        $user = new User();
        $user->user_type_id = 2;
        $user->institution_id = Auth::user()->institution_id;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = bcrypt($password);
        $user->save();

        Session::flash('success', 'User has been added');
        return redirect()->route('shop.user',encrypt($user->id));
    }

    /**
     *
     **/
    public function getUser($id)
    {
        return $id = decrypt($id);
    }

    /**
     *
     **/
    public function getUsers()
    {
        return view('shop.pages.users');
    }

    /**
     *
     **/
    public function getDeletedUsers()
    {
        return view('shop.pages.users');
    }

    /**
     *
     **/
    public function deleteUser($id)
    {
        //Decryption of ID
        $id = decrypt($id);

        User::find($id)->delete();

        Session::flash('success', 'User has been deleted.');
        return redirect()->route('shop.deleted_users');
    }

    /**
     *
     **/
    public function reinstateUser($id)
    {
        //Decryption of ID
        $id = decrypt($id);

        User::withTrashed()->find($id)->restore();

        Session::flash('success', 'User reinstated.');
        return $user = User::find($id);
    }
}
