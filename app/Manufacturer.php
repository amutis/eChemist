<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manufacturer extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function country()
    {
        return $this->hasOne('App\Country','id','country_id');
    }

    /**
     *
     **/
    public function drugs()
    {
        return $this->hasMany('App\Drug','manufacturer_id','id');
    }

    /**
     *
     **/
    public function user()
    {
        return $this->hasOne('App\User','institution_id','id');
    }
}
