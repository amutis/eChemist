<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function carts()
    {
        return $this->hasMany('App\Cart','order_id','id');
    }

    /**
     *
     **/
    public function payer()
    {
        return $this->hasOne('App\User','id','user_id');
    }
}
