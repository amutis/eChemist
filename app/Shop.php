<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function catalogues()
    {
        return $this->hasMany('App\Catalogue','shop_id','id');
    }

    /**
     *
     **/
    public function owner()
    {
        return $this->hasOne('App\User','institution_id','id');
    }

    /**
     *
     **/
    public function shop_type()
    {
        return $this->hasOne('App\ShopType','id','shop_type_id');
    }

    /**
     *
     **/
    public function country()
    {
        return $this->hasOne('App\Country','id','country_id');
    }
}
