<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopType extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function shop()
    {
        return $this->hasMany('App\Shop','shop_type_id','id');
    }
}
