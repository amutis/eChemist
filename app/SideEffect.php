<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SideEffect extends Model
{
    use SoftDeletes;
    /**
     *
     **/
    public function drug()
    {
        return $this->hasMany('App\Manufacturer','id','drug_id');
    }
}
