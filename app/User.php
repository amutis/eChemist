<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','user_type_id', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *
     **/
    public function carts()
    {
        return $this->hasMany('App\Cart','user_id','id');
    }

    /**
     *
     **/
    public function shop()
    {
        return $this->hasOne('App\Shop','id','shop_id');
    }

    /**
     *
     **/
    public function user_type()
    {
        return $this->hasOne('App\UserType','id','user_type_id');
    }
}
