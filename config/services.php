<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'paypal' => [
        'client_id' => 'Ac_PcwQvsP4_0ucrVjxICFUOr_xQJRFlseqNICWnkObrO4u4NULXpBUi-RBzJx0yHK_HN4R_r7E_PAaA',
        'secret' => 'EPXJYEKR8i-yE110gr_jvzORlXHiWYAIMmayg0U-SuSub3ni2FwsH3jA45uE_mJEfcHYIa6mOXVwo3zg'
    ],

];
