<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_type_id');
            $table->integer('country_id');
            $table->string('town');
            $table->string('region');
            $table->string('street');
            $table->string('building');
            $table->string('room_number')->nullable();
            $table->string('name');
            $table->string('telephone_1')->unique();
            $table->string('telephone_2')->nullable()->unique();
            $table->string('email')->unique();
            $table->string('website')->nullable()->unique();
            $table->time('opening_hours')->nullable();
            $table->time('closing_hours')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
