<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSideEffectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('side_effects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('drug_id');
            $table->text('side_effect_1')->nullable();
            $table->text('side_effect_2')->nullable();
            $table->text('side_effect_3')->nullable();
            $table->text('side_effect_4')->nullable();
            $table->text('side_effect_5')->nullable();
            $table->text('side_effect_6')->nullable();
            $table->text('side_effect_7')->nullable();
            $table->text('side_effect_8')->nullable();
            $table->text('side_effect_9')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('side_effects');
    }
}
