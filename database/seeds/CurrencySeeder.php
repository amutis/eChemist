<?php

use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->truncate();

        $currencies = [
            [
                'id' => 1,
                'name' => 'U.S. Dollar',
                'symbol' => '$',
                'code' => 'USD',
                'exchange_rate' => 1.00000000,
                'format' => '$ 1.00',
                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 2,
                'name' => 'Euro',
                'symbol' => '€',
                'code' => 'EUR',
                'exchange_rate' => 0.74970001,
                'format' => '€ 1.00',
                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 3,
                'name' => 'Pound Sterling',
                'symbol' => '£',
                'code' => 'GBP',
                'exchange_rate' => 0.62220001,
                'format' => '£ 1.00',
                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 4,
                'name' => 'Australian Dollar',
                'symbol' => '$',
                'code' => 'AUD',
                'exchange_rate' => 0.94790000,
                'format' => '$1.00',
                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 5,
                'name' => 'Canadian Dollar',
                'symbol' => '$',
                'code' => 'CAD',
                'exchange_rate' => 0.98500001,
                'format' => '$ 1.00',
                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 6,
                'name' => 'Czech Koruna',
                'symbol' => 'Kč',
                'code' => 'CZK',
                'exchange_rate' => 19.16900063,
                'format' => '1.00 Kč',
                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 7,
                'name' => 'Danish Krone',
                'symbol' => 'kr',

                'code' => 'DKK',
                'exchange_rate' => 5.59420013,
                'format' => 'kr 1.00',
                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 8,
                'name' => 'Hong Kong Dollar',
                'symbol' => '$',

                'code' => 'HKD',
                'exchange_rate' => 7.75290012,
                'format' => '$ 1.00',
                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 9,
                'name' => 'Hungarian Forint',
                'symbol' => 'Ft',

                'code' => 'HUF',

                'exchange_rate' => 221.27000427,
                'format' => 'Ft 1.00',

                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 10,
                'name' => 'Israeli New Sheqel',
                'symbol' => '?',

                'code' => 'ILS',

                'exchange_rate' => 3.73559999,
                'format' => '? 1.00',

                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 11,
                'name' => 'Japanese Yen',
                'symbol' => '¥',

                'code' => 'JPY',

                'exchange_rate' => 88.76499939,
                'format' => '¥ 1.00',

                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 12,
                'name' => 'Mexican Peso',
                'symbol' => '$',

                'code' => 'MXN',

                'exchange_rate' => 12.63899994,
                'format' => '$ 1.00',

                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 13,
                'name' => 'Norwegian Krone',
                'symbol' => 'kr',

                'code' => 'NOK',

                'exchange_rate' => 5.52229977,
                'format' => 'kr 1.00',

                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 14,
                'name' => 'New Zealand Dollar',
                'symbol' => '$',

                'code' => 'NZD',

                'exchange_rate' => 1.18970001,
                'format' => '$ 1.00',

                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 15,
                'name' => 'Philippine Peso',
                'symbol' => 'Php',

                'code' => 'PHP',

                'exchange_rate' => 40.58000183,
                'format' => 'Php 1.00',

                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 16,
                'name' => 'Polish Zloty',
                'symbol' => 'zł',
                'code' => 'PLN',
                'exchange_rate' => 3.08590007,
                'format' => '1.00 zł',
                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 17,
                'name' => 'Singapore Dollar',
                'symbol' => '$',
                'code' => 'SGD',
                'exchange_rate' => 1.22560000,
                'format' => '$ 1.00',
                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 18,
                'name' => 'Swedish Krona',
                'symbol' => 'kr',

                'code' => 'SEK',

                'exchange_rate' => 6.45870018,
                'format' => 'kr 1.00',

                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 19,
                'name' => 'Swiss Franc',
                'symbol' => 'CHF',

                'code' => 'CHF',

                'exchange_rate' => 0.92259997,
                'format' => 'CHF 1.00',

                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 20,
                'name' => 'Taiwan New Dollar',
                'symbol' => 'NT$',

                'code' => 'TWD',

                'exchange_rate' => 28.95199966,
                'format' => 'NT$ 1.00',

                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 21,
                'name' => 'Thai Baht',
                'symbol' => '฿',

                'code' => 'THB',

                'exchange_rate' => 30.09499931,
                'format' => '฿ 1.00',

                'active' => 1,
                'created_at' => '2013-11-29 19:51:38',
                'updated_at' => '2013-11-29 19:51:38',
            ],
            [
                'id' => 22,
                'name' => 'Ukrainian hryvnia',
                'symbol' => '₴',

                'code' => 'UAH',

                'exchange_rate' => 0.00,
                'format' => '₴ 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 23,
                'name' => 'Icelandic króna',
                'symbol' => 'kr',

                'code' => 'ISK',

                'exchange_rate' => 0.00,
                'format' => 'kr 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 24,
                'name' => 'Croatian kuna',
                'symbol' => 'kn',

                'code' => 'HRK',

                'exchange_rate' => 0.00,
                'format' => 'kn 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 25,
                'name' => 'Romanian leu',
                'symbol' => 'lei',

                'code' => 'RON',

                'exchange_rate' => 0.00,
                'format' => 'lei 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 26,
                'name' => 'Bulgarian lev',
                'symbol' => 'лв.',

                'code' => 'BGN',

                'exchange_rate' => 0.00,
                'format' => 'лв. 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 27,
                'name' => 'Turkish lira',
                'symbol' => '₺',

                'code' => 'TRY',

                'exchange_rate' => 0.00,
                'format' => '₺ 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 28,
                'name' => 'Chilean peso',
                'symbol' => '$',

                'code' => 'CLP',

                'exchange_rate' => 0.00,
                'format' => '$ 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 29,
                'name' => 'South African rand',
                'symbol' => 'R',

                'code' => 'ZAR',

                'exchange_rate' => 0.00,
                'format' => 'R 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 30,
                'name' => 'Brazilian real',
                'symbol' => 'R$',

                'code' => 'BRL',

                'exchange_rate' => 0.00,
                'format' => 'R$ 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 31,
                'name' => 'Malaysian ringgit',
                'symbol' => 'RM',

                'code' => 'MYR',

                'exchange_rate' => 0.00,
                'format' => 'RM 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 32,
                'name' => 'Russian ruble',
                'symbol' => '₽',

                'code' => 'RUB',

                'exchange_rate' => 0.00,
                'format' => '₽ 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 33,
                'name' => 'Indonesian rupiah',
                'symbol' => 'Rp',

                'code' => 'IDR',

                'exchange_rate' => 0.00,
                'format' => 'Rp 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 34,
                'name' => 'Indian rupee',
                'symbol' => '₹',

                'code' => 'INR',

                'exchange_rate' => 0.00,
                'format' => '₹ 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 35,
                'name' => 'Korean won',
                'symbol' => '₩',

                'code' => 'KRW',

                'exchange_rate' => 0.00,
                'format' => '₩ 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
            [
                'id' => 36,
                'name' => 'Renminbi',
                'symbol' => '¥',

                'code' => 'CNY',

                'exchange_rate' => 0.00,
                'format' => '¥ 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],[
                'id' => 37,
                'name' => 'Kenyan Shilling',
                'symbol' => 'Ksh',

                'code' => 'KES',

                'exchange_rate' => 0.00,
                'format' => 'Ksh 1.00',

                'active' => 1,
                'created_at' => '2015-07-22 23:25:30',
                'updated_at' => '2015-07-22 23:25:30',
            ],
        ];


        DB::table('currencies')->insert($currencies);
    }
}
