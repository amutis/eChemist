<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         $this->call(CountrySeeder::class);
         $this->call(UserSeeder::class);
         $this->call(UserTypeSeeder::class);
         $this->call(ShopType::class);
         $this->call(ManufacturerSeeder::class);
         $this->call(ShopSeeder::class);
         $this->call(GenericSeeder::class);
         $this->call(CurrencySeeder::class);
    }
}
