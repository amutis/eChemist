<?php

use Illuminate\Database\Seeder;

class GenericSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('generics')->truncate();

         $data = [
             ['name' => 'Prazosin Hydrochloride', 'description' => 'Treats high blood pressure'],
             ['name' => 'Clopidogrel =', 'description' => 'Used to reduce the risk of heart disease and stroke'],
             ['name' => 'Amlodipine', 'description' => 'Treats chest pains and other coronary atery diseases'],
             ['name' => 'Voglibose', 'description' => 'Delays the absorption of glucose thereby reducing the risk of macrovascular complications'],
             ['name' => 'Atorvastatin', 'description' => 'lowers cholesterol'],
             ['name' => 'Sitagliptin Phosphate & Metformin Hydrochloride', 'description' => 'Treating Type 2 diabetes that lowers blood sugar levels'],
             ['name' => 'Carvedilol', 'description' => 'Treating mild or severe heart failure'],
         ];

         DB::table('generics')->insert($data);
    }
}
