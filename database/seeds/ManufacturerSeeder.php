<?php

use Illuminate\Database\Seeder;

class ManufacturerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('manufacturers')->truncate();

         $data = [
             ['name' => 'Trial Manufacturer', 'country_id' => '116','address' => 'data','postal_code' => 'data','telephone_1' => 'data','email' => 'data','website' => 'data'],
         ];

         DB::table('manufacturers')->insert($data);
    }
}
