<?php

use Illuminate\Database\Seeder;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('shops')->truncate();

         $data = [
             ['shop_type_id' => '1','country_id' => '116', 'town' => 'data', 'region' => 'data', 'street' => 'data', 'building' => 'data', 'room_number' => 'data', 'name' => 'data', 'telephone_1' => 'data', 'email' => 'data', 'website' => 'data'],
         ];

         DB::table('shops')->insert($data);
    }
}
