<?php

use Illuminate\Database\Seeder;

class ShopType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('shop_types')->truncate();

         $data = [
             ['name' => 'Chemist', 'description' => 'data'],
             ['name' => 'Pharmacy', 'description' => 'data'],
             ['name' => 'Hospital', 'description' => 'data'],
         ];

         DB::table('shop_types')->insert($data);
    }
}
