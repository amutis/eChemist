<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->truncate();

         $data = [
             ['user_type_id' => '1','first_name' => 'User','last_name' => 'user','email' => 'user@gmail.com', 'password' => '$2y$10$/P4mTuKqVhuyzeYQL2yZHOIYsw.Vgpwkop6sqiM0Kw.5gWmVdDK/C'],
             ['user_type_id' => '2','first_name' => 'Chemist','last_name' => 'Owner','email' => 'chemist@gmail.com', 'password' => '$2y$10$/P4mTuKqVhuyzeYQL2yZHOIYsw.Vgpwkop6sqiM0Kw.5gWmVdDK/C'],
             ['user_type_id' => '3','first_name' => 'Manufacturer','last_name' => 'Account','email' => 'manu@gmail.com', 'password' => '$2y$10$/P4mTuKqVhuyzeYQL2yZHOIYsw.Vgpwkop6sqiM0Kw.5gWmVdDK/C'],
             ['user_type_id' => '4','first_name' => 'Admint','last_name' => 'Account','email' => 'admin@gmail.com', 'password' => '$2y$10$/P4mTuKqVhuyzeYQL2yZHOIYsw.Vgpwkop6sqiM0Kw.5gWmVdDK/C'],
         ];

         DB::table('users')->insert($data);
    }
}
