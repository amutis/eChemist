<?php

use Illuminate\Database\Seeder;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('user_types')->truncate();

         $data = [
             ['name' => 'Normal User', 'description' => 'Someone who just searches'],
             ['name' => 'Chemist Owner', 'description' => 'Someone who owns a chemist or pharmacy'],
             ['name' => 'Manufacturer', 'description' => 'Someone who manufacturers drugs'],
             ['name' => 'Admin', 'description' => 'Someone who oversees all that happens'],
         ];

         DB::table('user_types')->insert($data);
    }
}
