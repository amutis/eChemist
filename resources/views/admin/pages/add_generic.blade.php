@extends('admin.main')

@section('content')
        <!-- Content Header (Page header) -->
        <div class="col-md-7" style="margin-top: 2%">
        <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Generic</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(['route' => 'admin.add_generic']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Generic Name</label>
                        <input type="text" class="form-control" name="generic_name" value="{{old('generic_name')}}" id="exampleInputEmail1" placeholder="Generic Name...">
                        @if ($errors->has('generic_name'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('generic_name') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Description</label>
                        <textarea name="description" id="" cols="30" placeholder="Some description on the generic..." rows="10" class="form-control">
                            @if(old('description') != null)
                                {{old('description')}}
                            @endif
                        </textarea>
                        @if ($errors->has('description'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
                {!! Form::close() !!}
            </div>
        <!-- /.box -->
        </div>

@stop