@extends('admin.main')

@section('content')
       @if(Request::is('admin/cart/un_checked_out'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">All Drugs in cart</h3>
                           </div>
                           <div class="box">
                               <div class="box-header">
                                   <h3 class="box-title">Unchecked out</h3>
                               </div>
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>User</th>
                                           <th>Drug</th>
                                           <th>Generic</th>
                                           <th>Shop</th>
                                           <th>Price</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Cart::orderby('created_at','desc')->get() as $cart)
                                           <tr>
                                               <td>{{$cart->user->first_name}} {{$cart->user->last_name}}</td>
                                               <td>{{$cart->catalogue->drug->name}}</td>
                                               <td>{{$cart->catalogue->drug->generic->name}}</td>
                                               <td>{{$cart->catalogue->shop->name}}</td>
                                               <td>{{$cart->price}}</td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @elseif(Request::is('admin/cart/checked_out'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">All Drugs</h3>
                           </div>
                           <div class="box">
                               <div class="box-header">
                                   <h3 class="box-title">Checked out</h3>
                               </div>
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Order Number</th>
                                           <th>User</th>
                                           <th>Drug</th>
                                           <th>Generic</th>
                                           <th>Shop</th>
                                           <th>Price</th>
                                           {{--<th>Order To</th>--}}
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Cart::orderby('created_at','desc')->get() as $cart)
                                           <tr>
                                               <td>{{$cart->order_id}}</td>
                                               <td>{{$cart->user->first_name}} {{$cart->user->last_name}}</td>
                                               <td>{{$cart->catalogue->drug->name}}</td>
                                               <td>{{$cart->catalogue->drug->generic->name}}</td>
                                               <td>{{$cart->catalogue->budget->selling_price}}</td>
                                               <td>{{$cart->catalogue->budget->selling_price}}</td>
{{--                                               <td>{{$cart->order->name}}</td>--}}
                                               <td>
                                                   <a href=""><button class="btn btn-primary">View Order Details</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @endif
@stop