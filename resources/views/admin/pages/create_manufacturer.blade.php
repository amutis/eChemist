@extends('admin.main')

@section('content')
    <div class="row">
        <!-- Content Header (Page header) -->
        <div class="col-md-11" style="margin-left: 1%;margin-right: 1%;margin-top: 2%">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Register Manufacturer</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(['route' => 'admin.create_manufacturer']) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" class="form-control" name="name" value="{{old('name')}}" id="exampleInputEmail1" placeholder="Manufacturer Name...">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Country</label>
                                <select name="country" id="" class="form-control">
                                    @if(old('country') != null)
                                        <option value="{{old('country')}}">{{\App\Country::find(decrypt(old('country')))->name}}</option>
                                    @endif
                                    <option value="">Select Country</option>
                                    <option value="">----------------</option>
                                    @foreach(\App\Country::all() as $country)
                                        <option value="{{encrypt($country->id)}}">{{$country->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('country'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('country') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Address</label>
                                <input type="text" class="form-control" name="address" value="{{old('address')}}" id="exampleInputEmail1" placeholder="First Name...">
                                @if ($errors->has('address'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Postal Code</label>
                                <input type="text" class="form-control" name="postal_code" value="{{old('postal_code')}}" id="exampleInputEmail1" placeholder="Last Name...">
                                @if ($errors->has('postal_code'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('postal_code') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Telephone</label>
                                <input type="tel" class="form-control" name="telephone" value="{{old('telephone')}}" id="exampleInputEmail1" placeholder="Last Name...">
                                @if ($errors->has('telephone'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('telephone') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" name="manufacturer_email" value="{{old('manufacturer_email')}}" id="exampleInputEmail1" placeholder="Email...">
                                @if ($errors->has('manufacturer_email'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('manufacturer_email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h4>Manufacturer User Detail</h4>
                    <div class="form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}" id="exampleInputEmail1" placeholder="First Name...">
                        @if ($errors->has('first_name'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Last Name</label>
                        <input type="text" class="form-control" name="last_name" value="{{old('last_name')}}" id="exampleInputEmail1" placeholder="Last Name...">
                        @if ($errors->has('last_name'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Manager Email</label>
                        <input type="email" class="form-control" name="manager_email" value="{{old('manager_email')}}" id="exampleInputEmail1" placeholder="Email...">
                        @if ($errors->has('manager_email'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('manager_email') }}</strong>
                                </span>
                        @endif
                    </div>
                    <em>* Passwords are sent to the user automatically.</em>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Register</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->
        </div>
    </div>

@stop





























