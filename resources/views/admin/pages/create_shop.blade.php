@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="col-md-12" style="margin-top: 2%">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Register Shop</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'admin.create_shop']) !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Shop Name</label>
                    <input type="text" class="form-control" name="shop_name" value="{{old('shop_name')}}" id="exampleInputEmail1" placeholder="Shop Name...">
                    @if ($errors->has('shop_name'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('shop_name') }}</strong>
                                </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Shop Type</label>
                            <select name="shop_type" id="" class="form-control">
                                @if(old('shop_type') != null)
                                    <option value="{{old('shop_type')}}">{{\App\ShopType::find(decrypt(old('shop_type')))->name}}</option>
                                @endif
                                <option value="">Select Shop Type</option>
                                <option value="">----------------</option>
                                @foreach(\App\ShopType::all() as $type)
                                    <option value="{{encrypt($type->id)}}">{{$type->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('shop_type'))
                                <span class="help-block">
                            <strong>{{ $errors->first('shop_type') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Country</label>
                            <select name="country" id="" class="form-control">
                                @if(old('country') != null)
                                    <option value="{{old('country')}}">{{\App\Country::find(decrypt(old('country')))->name}}</option>
                                @endif
                                <option value="">Select Country</option>
                                <option value="">----------------</option>
                                @foreach(\App\Country::all() as $country)
                                    <option value="{{encrypt($country->id)}}">{{$country->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('country'))
                                <span class="help-block">
                            <strong>{{ $errors->first('country') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Town</label>
                            <input type="text" class="form-control" name="town" value="{{old('town')}}" id="exampleInputEmail1" placeholder="Town...">
                            @if ($errors->has('town'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('town') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Region</label>
                            <input type="text" class="form-control" name="region" value="{{old('region')}}" id="exampleInputEmail1" placeholder="Region in the town...">
                            @if ($errors->has('region'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('region') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Street</label>
                            <input type="text" class="form-control" name="street" value="{{old('street')}}" id="exampleInputEmail1" placeholder="Street in the region...">
                            @if ($errors->has('street'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('street') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Building</label>
                            <input type="text" class="form-control" name="building" value="{{old('building')}}" id="exampleInputEmail1" placeholder="Building on the Street...">
                            @if ($errors->has('building'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('building') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Telephone 1</label>
                            <input type="text" class="form-control" name="telephone_1" value="{{old('telephone_1')}}" id="exampleInputEmail1" placeholder="2547...">
                            @if ($errors->has('telephone_1'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('telephone_1') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Telephone 2</label>
                            <input type="text" class="form-control" name="telephone_2" value="{{old('telephone_2')}}" id="exampleInputEmail1" placeholder="2547 (optional)...">
                            @if ($errors->has('telephone_2'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('telephone_2') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Shop Email</label>
                            <input type="email" class="form-control" name="shop_email" value="{{old('shop_email')}}" id="exampleInputEmail1" placeholder="Shop Email...">
                            @if ($errors->has('shop_email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('shop_email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Website</label>
                            <input type="url" class="form-control" name="website" value="{{old('website')}}" id="exampleInputEmail1" placeholder="www.yourdomain.com (optional)...">
                            @if ($errors->has('website'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('website') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <hr>
                <h4>Shop Manager Details</h4>
                <div class="form-group">
                    <label for="exampleInputEmail1">First Name</label>
                    <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}" id="exampleInputEmail1" placeholder="First Name...">
                    @if ($errors->has('first_name'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Last Name</label>
                    <input type="text" class="form-control" name="last_name" value="{{old('last_name')}}" id="exampleInputEmail1" placeholder="Last Name...">
                    @if ($errors->has('last_name'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" name="manager_email" value="{{old('manager_email')}}" id="exampleInputEmail1" placeholder="Manager Email...">
                    @if ($errors->has('manager_email'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('manager_email') }}</strong>
                                </span>
                    @endif
                </div>


                <em>* Passwords are sent to the user automatically.</em>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Register</button>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>

@stop