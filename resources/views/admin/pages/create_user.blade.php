@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="col-md-7" style="margin-top: 2%">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Register User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'admin.user_create']) !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">User Type</label>
                    <select name="user_type" id="" class="form-control">
                        @if(old('user_type') != null)
                            <option value="{{old('user_type')}}">{{\App\UserType::find(decrypt(old('user_type')))->name}}</option>
                        @endif
                        <option value="">Select User Type</option>
                        <option value="">----------------</option>
                        @foreach(\App\UserType::all() as $type)
                            <option value="{{encrypt($type->id)}}">{{$type->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('user_type'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('user_type') }}</strong>
                                </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">First Name</label>
                    <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}" id="exampleInputEmail1" placeholder="First Name...">
                    @if ($errors->has('first_name'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Last Name</label>
                    <input type="text" class="form-control" name="last_name" value="{{old('last_name')}}" id="exampleInputEmail1" placeholder="Last Name...">
                    @if ($errors->has('last_name'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" name="email" value="{{old('email')}}" id="exampleInputEmail1" placeholder="Email...">
                    @if ($errors->has('email'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                    @endif
                </div>
                <em>* Passwords are sent to the user automatically.</em>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>

@stop