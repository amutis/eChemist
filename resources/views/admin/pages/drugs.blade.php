@extends('admin.main')

@section('content')
       @if(Request::is('admin/manufacturer/drugs/*'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">{{$manufacturer->name}}'s registered drugs</h3>
                           </div>
                           <div class="box">
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Generic Name</th>
                                           <th>Description</th>
                                           <th>Catalogue Occurrence</th>
                                           {{--<th>Action</th>--}}
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach($drugs as $drug)
                                           <tr>
                                               <td>{{$drug->name}}</td>
                                               <td>{{$drug->generic->name}}</td>
                                               <td>{{substr($drug->description,0,20)}}...</td>
                                               <td>{{\App\Catalogue::where('drug_id',$drug->id)->count()}}</td>
                                               {{--<td>--}}
                                                   {{--<a href=""><button class="btn btn-info">Edit</button></a>--}}
                                               {{--</td>--}}
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @elseif(Request::is('admin/deleted/users'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">All Users</h3>
                           </div>
                           <div class="box">
                               <div class="box-header">
                                   <h3 class="box-title">Inactive</h3>
                               </div>
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Email</th>
                                           <th>User Type</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\User::onlyTrashed()->orderby('created_at','desc')->get() as $user)
                                           <tr>
                                               <td>{{$user->first_name}} {{$user->last_name}}</td>
                                               <td>{{$user->email}}
                                               </td>
                                               <td>{{$user->user_type->name}}</td>
                                               <td>
                                                   <a href="{{route('admin.reinstate_user',encrypt($user->id))}}"><button class="btn btn-default">Reinstate</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @endif
@stop