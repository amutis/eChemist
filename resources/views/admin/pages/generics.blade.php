@extends('admin.main')

@section('content')
       @if(Request::is('admin/generics'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">All Generics</h3>
                           </div>
                           <div class="box">
                               <div class="box-header">
                                   <h3 class="box-title">Active</h3>
                               </div>
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Description</th>
                                           <th>Drugs in Catalogue</th>
                                           <th class="text-center">Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Generic::orderby('created_at','desc')->get() as $generic)
                                           <tr>
                                               <td>{{$generic->name}}</td>
                                               <td>{{substr($generic->description,0,20)}}...</td>
                                               <td>{{\App\Drug::where('generic_id',$generic->id)->count()}}</td>
                                               <td>
                                                   <a href=""><button class="btn btn-info">View Drugs</button></a>
                                                   <a href="{{route('admin.delete_generic',encrypt($generic->id))}}"><button class="btn btn-danger">Delete Generic</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @elseif(Request::is('admin/deleted/generics'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">All Generics</h3>
                           </div>
                           <div class="box">
                               <div class="box-header">
                                   <h3 class="box-title">Deleted</h3>
                               </div>
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Description</th>
                                           <th>Drugs that used generic</th>
                                           <th class="text-center">Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Generic::onlyTrashed()->orderby('deleted_at','desc')->get() as $generic)
                                           <tr>
                                               <td>{{$generic->name}}</td>
                                               <td>{{substr($generic->description,0,20)}}...</td>
                                               <td>{{\App\Drug::where('generic_id',$generic->id)->count()}}</td>
                                               <td>
                                                   <a href="{{route('admin.reinstate_generic',encrypt($generic->id))}}"><button class="btn btn-danger">Reinstate Generic</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @endif
@stop