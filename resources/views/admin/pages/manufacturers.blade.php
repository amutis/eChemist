@extends('admin.main')

@section('content')
       @if(Request::is('admin/manufacturers'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">All Active Manufacturers</h3>
                           </div>
                           <div class="box">
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Country</th>
                                           <th>Email</th>
                                           <th>Telephone</th>
                                           <th>Drugs</th>
                                           <th>Account Manager</th>
                                           {{--<th>Action</th>--}}
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Manufacturer::orderby('created_at','desc')->get() as $manufacturer)
                                           <tr>
                                               <td>{{$manufacturer->name}}</td>
                                               <td>{{$manufacturer->country->name}}</td>
                                               <td>{{$manufacturer->email}}</td>
                                               <td>{{$manufacturer->telephone_1}}</td>
                                               <td>{{\App\Drug::where('manufacturer_id',$manufacturer->id)->count()}}</td>
                                               <td>{{$manufacturer->user->first_name}} {{$manufacturer->user->last_name}} </td>
                                               <td>
                                                   {{--<a href=""><button class="btn btn-info">Edit</button></a>--}}
                                                   <a href="{{route('admin.manufacturer_drugs',encrypt($manufacturer->id))}}"><button class="btn btn-info">View Drugs</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @elseif(Request::is('admin/deleted/manufacturers'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">Deleted Manufacturers</h3>
                           </div>
                           <div class="box">
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Country</th>
                                           <th>Email</th>
                                           <th>Telephone</th>
                                           <th>Drugs</th>
                                           <th>Account Manager</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Manufacturer::onlyTrashed()->orderby('created_at','desc')->get() as $manufacturer)
                                           <tr>
                                               <td>{{$manufacturer->name}}</td>
                                               <td>{{$manufacturer->country->name}}</td>
                                               <td>{{$manufacturer->email}}</td>
                                               <td>{{$manufacturer->telephone_1}}</td>
                                               <td>{{\App\Drug::where('manufacturer_id',$manufacturer->id)->count()}}</td>
                                               <td>{{$manufacturer->user->first_name}} {{$manufacturer->user->last_name}} </td>
                                               <td>
                                                   <a href=""><button class="btn btn-default">Reinstate</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @endif
@stop