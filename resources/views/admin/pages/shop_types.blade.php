@extends('admin.main')

@section('content')
    <div class="row" style="margin-left: 2%">
        <!-- Content Header (Page header) -->
        <div class="col-md-5 " style="margin-top: 2%">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add User Type</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(['route' => 'admin.shop_types']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control" name="name" value="{{old('name')}}" id="exampleInputEmail1" placeholder="User Type Name...">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Description</label>
                        <textarea name="description" id="" cols="30" rows="10" class="form-control">
                            {{old('description')}}
                        </textarea>
                        @if ($errors->has('description'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-6" style="margin-top: 2%">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Shop Types Type</h3>
                </div>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(\App\ShopType::orderby('created_at','desc')->get() as $type)
                        <tr>
                            <td>{{$type->name}}</td>
                            <td>{{$type->description}}</td>
                            <td>
                                <a href=""><button class="btn btn-info">Edit</button></a>
                                <a href=""><button class="btn btn-danger">Delete</button></a>
                            </td>
                        </tr>
                    @endforeach

                    </tfoot>
                </table>
            </div>
        </div>
    </div>


@stop