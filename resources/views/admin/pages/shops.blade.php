@extends('admin.main')

@section('content')
       @if(Request::is('admin/shops'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">All Active Shops</h3>
                           </div>
                           <div class="box">
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Shop Type</th>
                                           <th>Country</th>
                                           <th>Town</th>
                                           <th>Email</th>
                                           <th>Telephone</th>
                                           <th>Catalogue</th>
                                           <th>Manager</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Shop::orderby('created_at','desc')->get() as $shop)
                                           <tr>
                                               <td>{{$shop->name}}</td>
                                               <td>{{$shop->shop_type->name}}</td>
                                               <td>{{$shop->country->name}}</td>
                                               <td>{{$shop->town}}</td>
                                               <td>{{$shop->email}}</td>
                                               <td>{{$shop->telephone_1}}</td>
                                               <td>{{\App\Catalogue::where('shop_id',$shop->id)->count()}}</td>
                                               <td>{{$shop->owner->first_name}} {{$shop->owner->last_name}}</td>
                                               <td>
                                                   <a href=""><button class="btn btn-info">View Shop</button></a>
                                                   <a href="{{route('admin.delete_shop',encrypt($shop->id))}}"><button class="btn btn-danger">Delete Shop</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @elseif(Request::is('admin/deleted/shops'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">All Users</h3>
                           </div>
                           <div class="box">
                               <div class="box-header">
                                   <h3 class="box-title">Inactive</h3>
                               </div>
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Shop Type</th>
                                           <th>Country</th>
                                           <th>Town</th>
                                           <th>Email</th>
                                           <th>Telephone</th>
                                           <th>Initial Catalogue</th>
                                           <th>Inital Manager</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Shop::onlyTrashed()->orderby('created_at','desc')->get() as $shop)
                                           <tr>
                                               <td>{{$shop->name}}</td>
                                               <td>{{$shop->shop_type->name}}</td>
                                               <td>{{$shop->country->name}}</td>
                                               <td>{{$shop->town}}</td>
                                               <td>{{$shop->email}}</td>
                                               <td>{{$shop->telephone_1}}</td>
                                               <td>{{\App\Catalogue::where('shop_id',$shop->id)->count()}}</td>
                                               <td>{{$shop->owner->first_name}} {{$shop->owner->last_name}}</td>
                                               <td>
                                                   <a href="{{route('admin.reinstate_shop',encrypt($shop->id))}}"><button class="btn btn-default">Reinstate Shop</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @endif
@stop