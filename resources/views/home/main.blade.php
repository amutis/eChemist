<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title>eChemist</title>
    <!-- Custom Theme files -->
    <link href="/search/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- Custom Theme files -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Flat Search Box Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <!--Google Fonts-->
    {{--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>--}}
    <!--Google Fonts-->
</head>
<body>
<!--search start here-->

<div class="search">
    <i> </i>
    <div class="s-bar">
        {!! Form::open(['route' => 'search']) !!}
        <input type="text" placeholder="Search Drug name/Generic Name" name="search_term" style="background: white; color: black">
        <input type="submit"  value="Search"/>
        @if ($errors->has('search_term'))
            <span class="help-block">
                        <strong style="color: white">{{ $errors->first('search_term') }}</strong>
                    </span>
        @endif
         {!! Form::close() !!}
    </div>
    <a href="{{route('manufacturers')}}"><button class="btn btn-primary">View Manufacturers</button></a>
    <a href="{{route('shops')}}"><button class="btn btn-primary">View Shops</button></a>
    @if(Auth::check())
        <a href="/logout"><button class="btn btn-primary">Logout</button></a>
    @else
        <a href="/login"><button class="btn btn-primary">Login</button></a>
        <a href="/register"><button class="btn btn-primary">Register</button></a>
    @endif

</div>
<!--search end here-->
@if(Auth::check())
    <div class="copyright">
        <p>Welcome back {{Auth::user()->first_name}}</p>
    </div>
@endif
</body>
</html>