<!DOCTYPE html>
<html>
<head>
   @include('home.partials.header')
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

    <header class="main-header">
       @include('home.partials.nav')
    </header>
    <!-- Full Width Column -->
    <div class="content-wrapper">
       @yield('content')
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.1
        </div>
        <strong>Copyright &copy; IS Project {{date('Y')}}</strong>  Mansi Patel
    </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<!-- Bootstrap 3.3.6 -->
<script src="/dashboard/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="/dashboard/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/dashboard/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/dashboard/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dashboard/dist/js/demo.js"></script>
</body>
</html>

