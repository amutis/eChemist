@extends('home.main2')

@section('content')
    <style>
        .value-minus,
        .value-plus,.value-minus1,
        .value-plus1{
            height: 40px;
            line-height: 24px;
            width: 40px;
            margin-right: 3px;
            display: inline-block;
            cursor: pointer;
            position: relative;
            font-size: 18px;
            color: #fff;
            text-align: center;
            -webkit-user-select: none;
            -moz-user-select: none;
            border:1px solid #b2b2b2;
            vertical-align: bottom;
        }
        .quantity-select .entry.value-minus:before,
        .quantity-select .entry.value-plus:before,.quantity-select .entry.value-minus1:before,
        .quantity-select .entry.value-plus1:before{
            content: "";
            width: 13px;
            height: 2px;
            background: #000;
            left: 50%;
            margin-left: -7px;
            top: 50%;
            margin-top: -0.5px;
            position: absolute;
        }
        .quantity-select .entry.value-plus:after,.quantity-select .entry.value-plus1:after{
            content: "";
            height: 13px;
            width: 2px;
            background: #000;
            left: 50%;
            margin-left: -1.4px;
            top: 50%;
            margin-top: -6.2px;
            position: absolute;
        }
        .value,.value1  {
            cursor: default;
            width: 40px;
            height:40px;
            padding: 8px 0px;
            color: #A9A9A9;
            line-height: 24px;
            border: 1px solid #E5E5E5;
            background-color: #E5E5E5;
            text-align: center;
            display: inline-block;
            margin-right: 3px;
        }
        .quantity-select .entry.value-minus:hover,
        .quantity-select .entry.value-plus:hover,.quantity-select .entry.value-minus1:hover,
        .quantity-select .entry.value-plus1:hover{
            background: #E5E5E5;
        }

        .quantity-select .entry.value-minus,.quantity-select .entry.value-minus1{
            margin-left: 0;
        }

    </style>
    <div class="row" style="margin-left: 2%; margin-right: 2%">
        <div class="wrapper" style="margin-top: 3%;background-color: white">
            <div>
                <img src="/ads/banner/" width="100%" alt="">
            </div>
            <div class="container">

                <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
                <div class="container">
                    <table id="cart" class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th style="width:50%">Product</th>
                            <th style="width:10%">Price</th>
                            <th style="width:15%">Quantity</th>
                            <th style="width:15%" class="text-center">Subtotal</th>
                            <th style="width:10%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($x=0)
                        @foreach(\App\Cart::where('user_id',Auth::user()->id)->where('status',0)->get() as $cart)
                        <tr id="row{{$x}}">
                            <td data-th="Product">
                                <div class="row">
                                    <div class="col-sm-2 hidden-xs"><img src="/drugs/{{$cart->catalogue->drug->image}}" alt="{{$cart->catalogue->drug->name}}" class="img-responsive"/></div>
                                    <div class="col-sm-10">
                                        <h4 class="nomargin">{{$cart->catalogue->drug->name}}</h4>
                                        <p>{{$cart->catalogue->drug->descrption}}</p>
                                    </div>
                                </div>
                            </td>
                            <td data-th="Price">Ksh {{$cart->price}}</td>
                            <td class="invert">
                                <div class="quantity">
                                    <div class="quantity-select">
                                        <div class="entry value-minus" id="minus{{$x}}">&nbsp;</div>
                                        <div class="entry value"><span>{{$cart->quantity}}</span></div>
                                        <div class="entry value-plus active" id="plus{{$x}}">&nbsp;</div>
                                    </div>
                                </div>
                            </td>
                            <script>$(document).ready(function(c) {
                                    $('#button{{$x}}').on('click', function(c){
                                        $.getJSON('{{route('cart.remove',array(encrypt($cart->id)))}}', function(){
                                        });
                                        $('#row{{$x}}').fadeOut('slow', function(c){
                                            $('#row{{$x}}').remove();
                                        });
                                        $.getJSON('{{route('cart.count')}}', function(all){
    //													alert (all);
                                            $("#cart_count").text(" "+all);
                                        });
                                        $.getJSON('{{route('cart.total')}}', function(total){
    //                                        var alltotal = total.TotalPrice;
    //									alert ("kjsdngk");
    //                                        console.log(total);
    //                                        $('#cart_total').text("Total Ksh "+alltotal)
                                        });
                                    });
                                });
                            </script>

                            <td data-th="Subtotal" class="text-center" id="product_total{{$x}}">Ksh {{$cart->amount*$cart->quantity}}</td>
                            <td class="actions" data-th="">
                                <button class="btn btn-danger btn-sm" id="button{{$x}}"><i class="fa fa-trash-o"></i></button>
                            </td>
                        </tr>
                        <script>
                            $('#plus{{$x}}').on('click', function(){
                                var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)+1;
                                divUpd.text(newVal);
                                $.getJSON('{{route('cart.quantity_add',array(encrypt($cart->id)))}}', function(total){
                                    $('#product_total{{$x}}').text("Ksh "+total)
                                });

                                $.getJSON('{{route('cart.total')}}', function(total){
                                    $('#cart_total').text("Total Ksh "+total)
                                });
                            });

                            $('#minus{{$x}}').on('click', function(){
                                var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)-1;
                                if(newVal>=1) divUpd.text(newVal);
                                $.getJSON('{{route('cart.quantity_minus',array(encrypt($cart->id)))}}', function(total){
                                    $('#product_total{{$x}}').text("Ksh "+total)
                                });

                                $.getJSON('{{route('cart.total')}}', function(total){
                                    $('#cart_total').text("Total Ksh "+total)
                                });
                            });
                        </script>
                        @php($x = $x+1)
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="visible-xs">
                            <td class="text-center"><strong >Total Ksh </strong></td>
                        </tr>
                        <tr>
                            <td><a href="/role" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                            <td colspan="2" class="hidden-xs"></td>
                            <td class="hidden-xs text-center"><strong id="cart_total">Total Ksh {{$total}}</strong></td>
                            <td><a href="{{route('make_order')}}" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div> <!-- end wrapper -->
    </div>
    </div>
@endsection