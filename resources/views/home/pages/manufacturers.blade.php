@extends('home.main2')

@section('content')

<div class="row" style="margin-left: 2%; margin-right: 2%">
    @foreach(\App\Manufacturer::all() as $manufacturer)
        <div class="col-md-4" style="margin-top: 2%">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$manufacturer->name}}</h3>
                </div>
                <div class="box-body">
                  <div class="row">
                      <div class="col-md-6">
                          <p>Country : {{$manufacturer->country->name}}</p>
                          <p>Address : {{$manufacturer->postal_code}}, {{$manufacturer->address}}</p>
                          <p>Telephone : {{$manufacturer->telephone}}</p>
                          <p>Email : {{$manufacturer->email}}</p>
                          <p>Website : <a href="{{$manufacturer->website}}">{{$manufacturer->website}}</a></p>
                      </div>
                      <div class="col-md-6">
                          Drugs registeredp : {{\App\Drug::where('manufacturer_id',$manufacturer->id)->count()}}
                      </div>
                  </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
@stop