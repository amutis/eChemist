@extends('home.main2')

@section('content')

<style>
    @import url(//fonts.googleapis.com/css?family=Lato:400,900);
    @import url(//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css);

    .animate {
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }
    .info-card {
        width: 100%;
        border: 1px solid rgb(215, 215, 215);
        position: relative;
        font-family: 'Lato', sans-serif;
        margin-bottom: 20px;
        overflow: hidden;
    }
    .info-card > img {
        width: 100px;
        margin-bottom: 60px;
    }
    .info-card .info-card-details,
    .info-card .info-card-details .info-card-header  {
        width: 100%;
        height: 100%;
        position: absolute;
        bottom: -100%;
        left: 0;
        padding: 0 15px;
        background: rgb(255, 255, 255);
        text-align: center;
    }
    .info-card .info-card-details::-webkit-scrollbar {
        width: 8px;
    }
    .info-card .info-card-details::-webkit-scrollbar-button {
        width: 8px;
        height: 0px;
    }
    .info-card .info-card-details::-webkit-scrollbar-track {
        background: transparent;
    }
    .info-card .info-card-details::-webkit-scrollbar-thumb {
        background: rgb(160, 160, 160);
    }
    .info-card .info-card-details::-webkit-scrollbar-thumb:hover {
        background: rgb(130, 130, 130);
    }

    .info-card .info-card-details .info-card-header {
        height: auto;
        bottom: 100%;
        padding: 10px 5px;
    }
    .info-card:hover .info-card-details {
        bottom: 0px;
        overflow: auto;
        padding-bottom: 25px;
    }
    .info-card:hover .info-card-details .info-card-header {
        position: relative;
        bottom: 0px;
        padding-top: 45px;
        padding-bottom: 25px;
    }
    .info-card .info-card-details .info-card-header h1,
    .info-card .info-card-details .info-card-header h3 {
        color: rgb(62, 62, 62);
        font-size: 22px;
        font-weight: 900;
        text-transform: uppercase;
        margin: 0 !important;
        padding: 0 !important;
    }
    .info-card .info-card-details .info-card-header h3 {
        color: rgb(142, 182, 52);
        font-size: 15px;
        font-weight: 400;
        margin-top: 5px;
    }
    .info-card .info-card-details .info-card-detail .social {
        list-style: none;
        padding: 0px;
        margin-top: 25px;
        text-align: center;
    }
    .info-card .info-card-details .info-card-detail .social a {
        position: relative;
        display: inline-block;
        min-width: 40px;
        padding: 10px 0px;
        margin: 0px 5px;
        overflow: hidden;
        text-align: center;
        background-color: rgb(215, 215, 215);
        border-radius: 40px;
    }


</style>
<div class="row" style="margin-left: 2%; margin-right: 2%">
    <div class="col-md-12" style="margin-top: 2%">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab">Drug Name bases results <span class="badge">{{$drugs->count()}}</span></a></li>
                <li><a href="#timeline" data-toggle="tab">Generic based Results <span class="badge">{{$generics->count()}}</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="activity">
                    @php($x = 1)
                    @foreach($drugs as $drug)
                        @foreach(\App\Catalogue::where('drug_id',$drug->id)->where('quantity','>',0)->get() as $catalogue)
                            <div class="col-md-3" style="margin-top: 2%">
                                <div class="info-card " >
                                    <img style="width: 100%" src="/drugs/{{$catalogue->drug->image}}" />
                                    <div class="info-card-details  animate" style="width: 100%">
                                        <div class="info-card-header ">
                                            <a href=""><h1>{{$catalogue->drug->name}}</h1></a>
                                            <a href=""><h3> Generic: {{$catalogue->drug->generic->name}} </h3></a>
                                            <h4> Ksh {{number_format($catalogue->budget->selling_price,0)}} </h4>
                                            <div class="row">
                                               @if(Auth::check())
                                                    <div class="col-md-4">
                                                        <button id="cart{{$x}}" class="btn btn-primary">Add to Cart</button>
                                                    </div>
                                                @else
                                                    <a href="/login">
                                                        <div class="col-md-4">
                                                            <button class="btn btn-primary">Login to Add to Cart</button>
                                                        </div>
                                                    </a>
                                                @endif
                                                <div class="col-md-4 col-md-offset-2">
                                                    <button class="btn btn-success">View Details</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="info-card-detail " >
                                            <!-- Description -->
                                            <p>{{substr($catalogue->drug->description,0,30)}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script>
                                $('#cart{{$x}}').click(function(){
                                    $.getJSON('{{route('cart.add',array(encrypt($catalogue->id)))}}', function(){
                                    });
                                    $.getJSON('{{route('cart.count')}}', function(all){
//                                        alert (all);
                                        $("#cart_count").text(" "+all);
                                        alert('{{$catalogue->drug->name}} has been added to cart');

                                    });

                                });

                            </script>
                            @php($x++)
                        @endforeach
                    @endforeach
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="timeline">
                    @foreach($generics as $generic)
                        @foreach(\App\Drug::where('generic_id',$generic->id)->get() as $drug)
                            @foreach(\App\Catalogue::where('drug_id',$drug->id)->where('quantity','>',0)->get() as $catalogue)
                                <div class="col-md-3" style="margin-top: 2%">
                                    <div class="info-card " >
                                        <img style="width: 100%" src="/drugs/{{$catalogue->drug->image}}" />
                                        <div class="info-card-details  animate" style="width: 100%">
                                            <div class="info-card-header ">
                                                <a href=""><h1>{{$catalogue->drug->name}}</h1></a>
                                                <a href=""><h3> Generic: {{$catalogue->drug->generic->name}} </h3></a>
                                                <h4> Ksh {{number_format($catalogue->budget->selling_price,0)}} </h4>
                                            </div>
                                            <div class="info-card-detail " >
                                                <!-- Description -->
                                                <p>{{substr($catalogue->drug->description,0,30)}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    @endforeach
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->

</div>
@stop