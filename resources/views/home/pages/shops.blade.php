@extends('home.main2')

@section('content')

<div class="row" style="margin-left: 2%; margin-right: 2%">
    @foreach(\App\Shop::all() as $shop)
        <div class="col-md-4" style="margin-top: 2%">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$shop->name}}</h3>
                </div>
                <div class="box-body">
                  <div class="row">
                      <div class="col-md-6">
                          <p>Country : {{$shop->country->name}}</p>
                          <p>Town : {{$shop->town}}</p>
                          <p>Region : {{$shop->region}}</p>
                          <p>Street : {{$shop->street}}</p>
                          <p>Building : {{$shop->building}}</p>
                      </div>
                      <div class="col-md-6">
                          Drugs types on Shelf : {{\App\Catalogue::where('shop_id',$shop->id)->count()}}
                      </div>
                  </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
@stop