<nav class="navbar navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a href="/" class="navbar-brand"><b>e</b>Chemist</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="{{route('manufacturers')}}">Manufacturers <span class="sr-only">(current)</span></a></li>
                <li><a href="{{route('shops')}}">Shops</a></li>
            </ul>
            {!! Form::open(['route' => 'search','class'=>'navbar-form navbar-left']) !!}
            <div class="form-group">
                <input type="text" name="search_term" class="form-control" id="navbar-search-input" placeholder="Search">
                <button  type="submit" class="btn btn-info">Search</button>
            </div>
            @if ($errors->has('search_term'))
                <span class="help-block">
                        <strong style="color: white">{{ $errors->first('search_term') }}</strong>
                    </span>
            @endif
            {!! Form::close() !!}
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
               @if(Auth::check())
                   <li><a href="{{route('cart')}}"><i class="fa fa-shopping-cart" id="cart_count"> {{\App\Cart::where('user_id',Auth::user()->id)->count()}}</i></a></li>
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                <p>
                                    {{Auth::user()->first_name}} {{Auth::user()->last_name}} - {{Auth::user()->user_type->name}}
                                    <small>Member since {{date('Y-m-d',strtotime(Auth::user()->created_at))}}</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
               @else
                    <li class=""><a href="/login">Login</a></li>
                    <li class=""><a href="/register">Create Account</a></li>
                @endif
            </ul>
        </div>
        <!-- /.navbar-custom-menu -->
    </div>
    <!-- /.container-fluid -->
</nav>