<!DOCTYPE html>
<html>
<head>
   @include('manufacturer.partials._header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

   @include('manufacturer.partials._top_nav')

    <!-- Left side column. contains the logo and sidebar -->
    @include('manufacturer.partials._side_nav')

    <!-- Content Wrapper. Contains page content -->
       <div class="content-wrapper">
            @yield('content')
        </div>
    <!-- /.content-wrapper -->
   @include('manufacturer.partials._footer')

</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="/dashboard/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->


@yield('script')

<!-- FastClick -->
<script src="/dashboard/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/dashboard/dist/js/app.min.js"></script>
<!-- AdminLTE admin demo (This is only for demo purposes) -->
<script src="/dashboard/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dashboard/dist/js/demo.js"></script>
</body>
</html>

