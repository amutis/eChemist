@extends('manufacturer.main')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="col-md-7" style="margin-top: 2%">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Register Drug</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'manufacturer.add_drug','files'=>true]) !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Generic Name</label>
                    <select name="generic_name" id="" class="form-control">
                        @if(old('generic_name') != null)
                            <option value="{{old('generic_name')}}">{{\App\Generic::find(decrypt(old('generic_name')))->name}}</option>
                        @endif
                        <option value="">Select Generic</option>
                        <option value="">----------------</option>
                        @foreach(\App\Generic::all() as $generic)
                            <option value="{{encrypt($generic->id)}}">{{$generic->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('generic_name'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('generic_name') }}</strong>
                                </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1"> Name</label>
                    <input type="text" class="form-control" name="name" value="{{old('name')}}" id="exampleInputEmail1" placeholder="Drug Name...">
                    @if ($errors->has('name'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1"> Image</label>
                    <input type="file" class="form-control" name="image" value="{{old('image')}}" >
                    @if ($errors->has('image'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Description</label>
                    <textarea name="description" id="" cols="30" rows="10" class="form-control">
                        {{old('description')}}
                    </textarea>
                    @if ($errors->has('description'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                    @endif
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>

@stop