@extends('manufacturer.main')

@section('content')
       @if(Request::is('manufacturer/drugs'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">Registered drugs</h3>
                           </div>
                           <div class="box">
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Generic Name</th>
                                           <th>Description</th>
                                           <th>Catalogue Occurrence</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Drug::where('manufacturer_id',Auth::user()->institution_id)->orderby('created_at','desc')->get() as $drug)
                                           <tr>
                                               <td>{{$drug->name}}</td>
                                               <td>{{$drug->generic->name}}</td>
                                               <td>{{substr($drug->description,0,20)}}...</td>
                                               <td>{{\App\Catalogue::where('drug_id',$drug->id)->count()}}</td>
                                               <td>
                                                   <a href=""><button class="btn btn-info">Edit</button></a>
                                                   <a href="{{route('manufacturer.delete_drug',encrypt($drug->id))}}"><button class="btn btn-danger">Delete</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @elseif(Request::is('manufacturer/deleted/drugs'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">Deleted Drugs</h3>
                           </div>
                           <div class="box">
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Generic Name</th>
                                           <th>Description</th>
                                           <th>Catalogue Occurrence</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Drug::onlyTrashed()->where('manufacturer_id',Auth::user()->institution_id)->orderby('created_at','desc')->get() as $drug)
                                           <tr>
                                               <td>{{$drug->name}}</td>
                                               <td>{{$drug->generic->name}}</td>
                                               <td>{{substr($drug->description,0,20)}}...</td>
                                               <td>{{\App\Catalogue::where('drug_id',$drug->id)->count()}}</td>
                                               <td>
                                                   <a href="{{route('manufacturer.reinstate_drug',encrypt($drug->id))}}"><button class="btn btn-default">Reinstate</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @endif
@stop