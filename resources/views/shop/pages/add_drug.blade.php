@extends('shop.main')

@section('content')
    <div class="row" style="margin-left: 2%; margin-right: 2%; ">
        <!-- Content Header (Page header) -->
        <div class="col-md-12" style="margin-top: 2%">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Register Drug</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(['route' => 'shop.add_drug']) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Manufacturer</label>
                                <select name="manufacturer" id="" class="form-control">
                                    @if(old('manufacturer') != null)
                                        <option value="{{old('manufacturer')}}">{{\App\Manufacturer::find(decrypt(old('manufacturer')))->name}}</option>
                                    @endif
                                    <option value="">Select Manufacturer</option>
                                    <option value="">----------------</option>
                                    @foreach(\App\Manufacturer::all() as $manufacturer)
                                        <option value="{{encrypt($manufacturer->id)}}">{{$manufacturer->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('manufacturer'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('manufacturer') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Drug</label>
                                <select name="drug" id="" class="form-control">
                                    @if(old('drug') != null)
                                        <option value="{{old('drug')}}">{{\App\Manufacturer::find(decrypt(old('drug')))->name}}</option>
                                    @endif
                                    <option value="">Select Drug</option>
                                    <option value="">----------------</option>
                                    @foreach(\App\Drug::all() as $drug)
                                        <option value="{{encrypt($drug->id)}}">{{$drug->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('drug'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('drug') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1"> Quantity</label>
                                <input type="number" class="form-control" name="quantity" value="{{old('quantity')}}" id="exampleInputEmail1" placeholder="Quantity...">
                                @if ($errors->has('quantity'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('quantity') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1"> Buying Price</label>
                                <input type="number" class="form-control" name="buying_price" value="{{old('buying_price')}}" id="exampleInputEmail1" placeholder="Buying Price...">
                                @if ($errors->has('buying_price'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('buying_price') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1"> Selling Price</label>
                                <input type="number" class="form-control" name="selling_price" value="{{old('selling_price')}}" id="exampleInputEmail1" placeholder="Selling Price...">
                                @if ($errors->has('selling_price'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('selling_price') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Add to Catalogue</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->
        </div>
    </div>
@stop