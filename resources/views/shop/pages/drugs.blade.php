@extends('shop.main')

@section('content')
       @if(Request::is('shop/drugs'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">Registered drugs</h3>
                           </div>
                           <div class="box">
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Generic Name</th>
                                           <th>Description</th>
                                           <th>Current Orders</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Catalogue::where('shop_id',Auth::user()->institution_id)->orderby('created_at','desc')->get() as $catalogue)
                                           <tr>
                                               <td>{{$catalogue->drug->name}}</td>
                                               <td>{{$catalogue->drug->generic->name}}</td>
                                               <td>{{substr($catalogue->drug->description,0,20)}}...</td>
                                               <td>{{\App\Cart::where('catalogue_id',$catalogue->id)->where('status',0)->count()}}</td>
                                               <td>
                                                   <a href=""><button class="btn btn-info">Edit</button></a>
                                                   <a href="{{route('shop.delete_drug',encrypt($catalogue->id))}}"><button class="btn btn-danger">Delete</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @elseif(Request::is('shop/deleted/drugs'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">Deleted Drugs</h3>
                           </div>
                           <div class="box">
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Generic Name</th>
                                           <th>Description</th>
                                           <th>Current Orders</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Catalogue::onlyTrashed()->where('shop_id',Auth::user()->institution_id)->orderby('created_at','desc')->get() as $catalogue)
                                           <tr>
                                               <td>{{$catalogue->drug->name}}</td>
                                               <td>{{$catalogue->drug->generic->name}}</td>
                                               <td>{{substr($catalogue->drug->description,0,20)}}...</td>
                                               <td>{{\App\Cart::onlyTrashed()->where('catalogue_id',$catalogue->id)->where('status',0)->count()}}</td>
                                               <td>
                                                   <a href="{{route('shop.reinstate_drug',encrypt($catalogue->id))}}"><button class="btn btn-default">Reinstate</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @endif
@stop