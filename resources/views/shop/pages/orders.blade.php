@extends('shop.main')

@section('content')
       @if(Request::is('shop/orders/uncomplete'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">All Uncomplete Orders</h3>
                           </div>
                           <div class="box">
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Name</th>
                                           <th>Country</th>
                                           <th>Location</th>
                                           <th>Email</th>
                                           <th>Phone Number</th>
                                           <th>Urgency</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Order::orderby('created_at','desc')->get() as $order)
                                           <tr>
                                               <td>{{$order->name}}</td>
                                               <td>{{$order->country->name}}</td>
                                               <td>{{$order->location}}</td>
                                               <td>{{$order->email}}</td>
                                               <td>{{$order->phone_number}}</td>
                                               <td>{{$order->urgency}}</td>
                                               <td>
                                                   <a href=""><button class="btn btn-info">Remind Shops</button></a>
                                               </td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @elseif(Request::is('shop/orders/complete'))
           <!-- Content Header (Page header) -->
           <section class="content-header">
               <div class="row">
                   <div class="col-xs-12">
                       <div class="box">
                           <div class="box-header">
                               <h3 class="box-title">Complete Orders</h3>
                           </div>
                           <div class="box">
                               <!-- /.box-header -->
                               <div class="box-body">
                                   <table id="example1" class="table table-bordered table-striped">
                                       <thead>
                                       <tr>
                                           <th>Delivery Date</th>
                                           <th>Name</th>
                                           <th>Country</th>
                                           <th>Location</th>
                                           <th>Email</th>
                                           <th>Phone Number</th>
                                           <th>Urgency</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @foreach(\App\Order::orderby('created_at','desc')->get() as $order)
                                           <tr>
                                               <td>{{date('d M. Y',strtotime($order->updated_at))}}</td>
                                               <td>{{$order->name}}</td>
                                               <td>{{$order->country->name}}</td>
                                               <td>{{$order->location}}</td>
                                               <td>{{$order->email}}</td>
                                               <td>{{$order->phone_number}}</td>
                                               <td>{{$order->urgency}}</td>
                                           </tr>
                                       @endforeach

                                       </tfoot>
                                   </table>
                               </div>
                               <!-- /.box-body -->
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       @endif
@stop