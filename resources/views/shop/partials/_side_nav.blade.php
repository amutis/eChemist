<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
                <a href="{{route('shop.dashboard')}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Users</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right">{{\App\User::where('user_type_id',2)->where('institution_id',Auth::user()->institution_id)->count()}}</span>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('shop.user_create')}}"><i class="fa fa-circle-o"></i> Register</a></li>
                    <li><a href="{{route('shop.users')}}"><i class="fa fa-circle-o"></i> Users</a></li>
                    <li><a href="{{route('shop.deleted_users')}}"><i class="fa fa-circle-o"></i> Deleted Users</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>Catalogue</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('shop.add_drug')}}"><i class="fa fa-circle-o"></i> Add Drug</a></li>
                    <li><a href="{{route('shop.drugs')}}"><i class="fa fa-circle-o"></i> List</a></li>
                    <li><a href="{{route('shop.deleted_drugs')}}"><i class="fa fa-circle-o"></i> Deleted Generics</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-truck"></i>
                    <span>Orders</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('shop.uncomplete_orders')}}"><i class="fa fa-circle-o"></i> Uncomplete Orders</a></li>
                    <li><a href="{{route('shop.complete_orders')}}"><i class="fa fa-circle-o"></i> Complete Orders</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>