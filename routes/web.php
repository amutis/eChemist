<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');



Auth::routes();

Route::get('/', 'Home\Home@index');
Route::get('/role', 'Role@check');
Route::get('manufacturers', 'Home\ManufacturersController@all')->name('manufacturers');
Route::get('shops', 'Home\ShopsController@all')->name('shops');

Route::get('search/result', 'Home\Search@result')->name('home.search');
Route::post('search/result', 'Home\Search@search')->name('search');

/*
 * Json Routes
 */
Route::get('add/cart/{id}', 'Home\Jquey@addToCart')->name('cart.add');
Route::get('cart/count', 'Home\Jquey@countCart')->name('cart.count');
Route::get('cart', 'Home\CartController@getCart')->name('cart');
Route::get('cart/add/{cart}', 'Home\Jquey@cart_add')->name('cart.quantity_add');
Route::get('cart/total', 'Home\Jquey@cart_total')->name('cart.total');
Route::get('cart/subtract/{cart}', 'Home\Jquey@cart_subtract')->name('cart.quantity_minus');
Route::get('cart/remove/{cart}', 'Home\Jquey@remove')->name('cart.remove');
Route::get('cart/checkout', 'Home\PaypalTransactions@post_order')->name('make_order');


//Paypal
Route::get('paypal/done/{amount}', 'PaypalTransactions@getDone')->name('get_done');
Route::get('paypal/cancel', 'PaypalTransactions@getCancel')->name('get_cancel');


/*
 * Admin routes
 */

Route::get('admin/dashboard', 'Admin\Dashboard@dashboard')->name('admin.dashboard');
Route::get('admin/user/create', 'Admin\Dashboard@getUserCreate')->name('admin.user_create');
Route::post('admin/user/create', 'Admin\User@postUser')->name('admin.user_create');
Route::get('admin/users', 'Admin\Dashboard@getUsers')->name('admin.users');
Route::get('admin/user/{id}', 'Admin\User@getUser')->name('admin.user');
Route::get('admin/user_types', 'Admin\User@getUserTypes')->name('admin.user_types');
Route::post('admin/user_types', 'Admin\User@postUserType')->name('admin.user_type');
Route::get('admin/deleted/users', 'Admin\User@deletedUser')->name('admin.deleted_users');

Route::get('admin/reinstate/user/{id}', 'Admin\User@reinstateUser')->name('admin.reinstate_user');
Route::get('admin/delete/user/{id}', 'Admin\User@deleteUser')->name('admin.delete_user');

Route::get('admin/generic/add', 'Admin\Generic@getCreate')->name('admin.add_generic');
Route::post('admin/generic/add', 'Admin\Generic@postGeneric')->name('admin.add_generic');
Route::get('admin/generics', 'Admin\Generic@getGenerics')->name('admin.generics');
Route::get('admin/deleted/generics', 'Admin\Generic@getDeletedGenerics')->name('admin.deleted_generics');
Route::get('admin/delete/generic/{id}', 'Admin\Generic@deleteGeneric')->name('admin.delete_generic');
Route::get('admin/reinstate/generic/{id}', 'Admin\Generic@reinstateGeneric')->name('admin.reinstate_generic');

Route::get('admin/cart/checked_out', 'Admin\Cart@getCheckedOut')->name('admin.checked_out');
Route::get('admin/cart/un_checked_out', 'Admin\Cart@getUnCheckedOut')->name('admin.unchecked_out');


Route::get('admin/orders/uncomplete', 'Admin\Order@getUncomplete')->name('admin.uncomplete_orders');
Route::get('admin/orders/complete', 'Admin\Order@getComplete')->name('admin.complete_orders');


Route::get('admin/shop/add', 'Admin\Shop@getCreateShop')->name('admin.create_shop');
Route::post('admin/shop/add', 'Admin\Shop@postShop')->name('admin.create_shop');
Route::get('admin/shops', 'Admin\Shop@getShops')->name('admin.shops');
Route::get('admin/shop/{id}', 'Admin\Shop@getShop')->name('admin.shop');
Route::get('admin/shop_type', 'Admin\Shop@getShopTypes')->name('admin.shop_types');
Route::post('admin/shop_type', 'Admin\Shop@postShopType')->name('admin.shop_types');
Route::get('admin/deleted/shops', 'Admin\Shop@getDeletedShops')->name('admin.deleted_shops');
Route::get('admin/delete/shop/{id}', 'Admin\Shop@deleteShop')->name('admin.delete_shop');
Route::get('admin/reinstate/shop/{id}', 'Admin\Shop@reinstateShop')->name('admin.reinstate_shop');


Route::get('admin/manufacturer/add', 'Admin\Manufacturer@getCreateManufacturer')->name('admin.create_manufacturer');
Route::post('admin/manufacturer/add', 'Admin\Manufacturer@postManufacturer')->name('admin.create_manufacturer');
Route::get('admin/manufacturers', 'Admin\Manufacturer@getManufacturers')->name('admin.manufacturers');
Route::get('admin/manufacturer/{id}', 'Admin\Manufacturer@getManufacturer')->name('admin.manufacturer');
Route::get('admin/deleted/manufacturers', 'Admin\Manufacturer@getDeletedManufacturer')->name('admin.deleted_manufacturers');
Route::get('admin/delete/manufacturer/{id}', 'Admin\Manufacturer@deleteManufacturer')->name('admin.delete_manufacturer');
Route::get('admin/reinstate/manufacturer/{id}', 'Admin\Manufacturer@reinstateManufacturer')->name('admin.reinstate_manufacturer');
Route::get('admin/manufacturer/drugs/{id}', 'Admin\Drugs@getManufacturerDrugs')->name('admin.manufacturer_drugs');


/*
 * Manufacturer Routes
 */


Route::get('manufacturer/dashboard', 'Manufacturer\Dashboard@dashboard')->name('manufacturer.dashboard');
Route::get('manufacturer/user/create', 'Manufacturer\Users@getUserCreate')->name('manufacturer.user_create');
Route::post('manufacturer/user/create', 'Manufacturer\Users@postUser')->name('manufacturer.user_create');
Route::get('manufacturer/users', 'Manufacturer\Users@getUsers')->name('manufacturer.users');
Route::get('manufacturer/user/{id}', 'Manufacturer\Users@getUser')->name('manufacturer.user');
Route::get('manufacturer/deleted/users', 'Manufacturer\Users@getDeletedUsers')->name('manufacturer.deleted_users');

Route::get('manufacturer/reinstate/user/{id}', 'Manufacturer\Users@reinstateUser')->name('manufacturer.reinstate_user');
Route::get('manufacturer/delete/user/{id}', 'Manufacturer\Users@deleteUser')->name('manufacturer.delete_user');


Route::get('manufacturer/drug/add', 'Manufacturer\Drugs@getCreateDrug')->name('manufacturer.add_drug');
Route::post('manufacturer/drug/add', 'Manufacturer\Drugs@postDrug')->name('manufacturer.add_drug');
Route::get('manufacturer/drugs', 'Manufacturer\Drugs@getDrugs')->name('manufacturer.drugs');
Route::get('manufacturer/deleted/drugs', 'Manufacturer\Drugs@getDeletedDrugs')->name('manufacturer.deleted_drugs');
Route::get('manufacturer/delete/drug/{id}', 'Manufacturer\Drugs@deleteDrug')->name('manufacturer.delete_drug');
Route::get('manufacturer/reinstate/drug/{id}', 'Manufacturer\Drugs@reinstateDrug')->name('manufacturer.reinstate_drug');




/*
 * Shop Routes
 */


Route::get('shop/dashboard', 'Shop\Dashboard@dashboard')->name('shop.dashboard');
Route::get('shop/user/create', 'Shop\Users@getUserCreate')->name('shop.user_create');
Route::post('shop/user/create', 'Shop\Users@postUser')->name('shop.user_create');
Route::get('shop/users', 'Shop\Users@getUsers')->name('shop.users');
Route::get('shop/user/{id}', 'Shop\Users@getUser')->name('shop.user');
Route::get('shop/deleted/users', 'Shop\Users@getDeletedUsers')->name('shop.deleted_users');

Route::get('shop/reinstate/user/{id}', 'Shop\Users@reinstateUser')->name('shop.reinstate_user');
Route::get('shop/delete/user/{id}', 'Shop\Users@deleteUser')->name('shop.delete_user');


Route::get('shop/drug/add', 'Shop\Catalogues@getCreateDrug')->name('shop.add_drug');
Route::post('shop/drug/add', 'Shop\Catalogues@postDrug')->name('shop.add_drug');
Route::get('shop/drugs', 'Shop\Catalogues@getDrugs')->name('shop.drugs');
Route::get('shop/deleted/drugs', 'Shop\Catalogues@getDeletedDrugs')->name('shop.deleted_drugs');
Route::get('shop/delete/drug/{id}', 'Shop\Catalogues@deleteDrug')->name('shop.delete_drug');
Route::get('shop/reinstate/drug/{id}', 'Shop\Catalogues@reinstateDrug')->name('shop.reinstate_drug');


Route::get('shop/orders/uncomplete', 'Shop\Orders@getUnComplete')->name('shop.uncomplete_orders');
Route::get('shop/orders/complete', 'Shop\Orders@getComplete')->name('shop.complete_orders');
